window.addEventListener("DOMContentLoaded",()=>{

    const toggleSwitch=document.getElementById("toggle-switch")
    toggleSwitch.addEventListener("change",(event)=>{

        if(event.target.checked){
            document.getElementById('basic').innerText = 19.99;
            document.getElementById('professional').innerText = 24.99;
            document.getElementById('master').innerText = 39.99;        
        }
        else{
            document.getElementById('basic').innerText = 199.99;
            document.getElementById('professional').innerText = 249.99;
            document.getElementById('master').innerText = 399.99;
        }
        
    })
    
})
